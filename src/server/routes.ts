import * as express from 'express';
import UserControlller = require("./controller/user.controller");

export function init(app: express.Application) {
  //services.init(app);
  let userController = new UserControlller();

  app.put("/api/create/:id",userController.createRecord);
  app.post("/api/update/:id",userController.updateRecord);
  app.delete("/api/delete/:id",userController.deleteRecord);
  app.get("/api/fetch/:id",userController.getRecord);
}
