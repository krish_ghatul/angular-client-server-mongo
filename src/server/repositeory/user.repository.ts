
import RepositoryBase = require("./base/RepositoryBase");
import User = require("../dataaccess/mongoose/user");
import schema = require("../dataaccess/schemas/user.schema");

class UserRepository extends RepositoryBase<User> {
  constructor() {
    super(schema);
  }

}

Object.seal(UserRepository);
export = UserRepository;
