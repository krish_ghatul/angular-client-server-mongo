import DataAccess = require('../dataaccess');
import User = require('../mongoose/user');

let mongoose = DataAccess.mongooseInstance;
let mongooseConnection = DataAccess.mongooseConnection;


class UserSchema {
  static get schema() {
    let schema = mongoose.Schema({
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      email: {
        type: String,
        required: true,
        unique: true
      },
      mobile_number: {
        type: Number,
        required: true,
        unique: true
      }
    }, {versionKey: false});
    return schema;
  }
}
let schema = mongooseConnection.model<User>('User', UserSchema.schema);
export = schema;
