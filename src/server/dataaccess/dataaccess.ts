import * as Mongoose from 'mongoose';
let config = require('config');

class DataAccess {
  static mongooseInstance: any;
  static mongooseConnection: Mongoose.Connection;


  static connect(): Mongoose.Connection {
    if (this.mongooseInstance) return this.mongooseInstance;

    this.mongooseConnection = Mongoose.connection;
    this.mongooseConnection.once('open', () => {
      console.log('Connected to mongodb.');
    });

    let host = config.get('gk-seed.database.host');
    let name = config.get('gk-seed.database.name');
    Mongoose.set('debug',true);
    this.mongooseInstance = Mongoose.connect('mongodb://' + host + '/' + name+'');
    return this.mongooseInstance;
  }
}
export = DataAccess;
