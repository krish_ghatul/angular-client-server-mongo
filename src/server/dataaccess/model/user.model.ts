interface UserModel {
  first_name: string;
  last_name: string;
  email: string;
  mobile_number: number;
}
export = UserModel;
